#HSLIDE?image=assets/img/pexels-photo-220357.jpeg

<h2 style="color: rgb(185, 196, 0) !important">Kolloquium</h2>

<br>

<span class="subheader">Bachelorthesis</span>

#### "Architektur für JavaScript-basierte Web-Frontends mit komplexem Anwendungszustand"
<br>

#HSLIDE

### Problemstellung

- Evolution von JavaScript <!-- .element: class="fragment" data-fragment-index="1" -->
- Verlagerung der Logik auf die Clientseite <!-- .element: class="fragment" data-fragment-index="2" -->
- mehr Anforderungen <!-- .element: class="fragment" data-fragment-index="3" -->
- viele Features <!-- .element: class="fragment" data-fragment-index="4" -->
- Mobile Endgeräte <!-- .element: class="fragment" data-fragment-index="5" -->

#VSLIDE?image=assets/img/black-and-white-code-programming-tech-79290.jpeg
<h2 style="color: #EF5350 !important; text-shadow: 4px 4px 16px #101010 !important;">Komlexität</h2>

#VSLIDE?image=assets/img/black-and-white-code-programming-tech-79290.jpeg
<!-- .slide: data-background-transition="none" -->
<h2 style="color: #EF5350 !important; text-shadow: 4px 4px 16px #101010 !important">Nachvollziehbarkeit</h2>

#VSLIDE?image=assets/img/black-and-white-code-programming-tech-79290.jpeg
<!-- .slide: data-background-transition="none" -->
<h2 style="color: #EF5350 !important; text-shadow: 4px 4px 16px #101010 !important">Fehler beheben</h2>

#VSLIDE?image=assets/img/black-and-white-code-programming-tech-79290.jpeg
<!-- .slide: data-background-transition="none" -->
<h2 style="color: #EF5350 !important; text-shadow: 4px 4px 16px #101010 !important">Testbarkeit</h2>

#VSLIDE?image=assets/img/black-and-white-code-programming-tech-79290.jpeg
<!-- .slide: data-background-transition="none" -->
<h2 style="color: #EF5350 !important; text-shadow: 4px 4px 16px #101010 !important">Single Page Applications</h2>

#HSLIDE
### Anforderungen

#VSLIDE
<h3>Login und Sicherheit</h3> 
<ul> 
    <li>Authentifizierung</li> 
    <li>Autorisierung</li> 
    <li>Zustände zurücksetzen</li> 
    <li>sichere Übertragung zum Server</li> 
</ul>

#VSLIDE
<h3>Routing und Navigation</h3> 
<ul> 
    <li>Absicherung der Routen</li> 
    <li>Eindeutige URLs</li> 
    <li>Weiterleitungen</li> 
</ul>

#VSLIDE
<h3>Synchronisation mit dem Server</h3> 
<ul> 
    <li>Asynchronität</li> 
    <li>Zustände in DB speichern</li> 
    <li>Fehlermeldungen</li> 
</ul>

#VSLIDE
<h3>Internationalisierung</h3>
<ul>
    <li>Sprache ändern</li>
    <li>Übersetzungen laden</li>
</ul>

#VSLIDE
<h3>Komplexe Formulare</h3> 
<ul> 
    <li>Eingaben in mehreren Komponenten</li> 
    <li>Zustände zurücksetzen</li> 
    <li>Validierung</li> 
</ul>

#VSLIDE
<h3>Deaktivieren von Oberflächenelementen</h3>
<ul>
    <li>Komponenten ausblenden</li>
    <li>DOM Rendering</li>
</ul>

#VSLIDE
<h3>Integration von Drittbibliotheken</h3>
<ul>
    <li>Charts</li>
    <li>i18n</li>
    <li>Routing</li>
    <li>Logging</li>
</ul>

#VSLIDE
<h3>Anpassbarkeit und Wartung</h3>
<ul>
    <li>neue Kundenanforderungen</li>
    <li>neue Features</li>
    <li>neue Zustände</li>
</ul>

#VSLIDE
<h3>Entwicklerwerkzeuge</h3>
<ul>
    <li>Testbarkeit</li>
    <li>Übersicht über alle Zustände</li>
    <li>Hot Reloading</li>
    <li>Nachvollziehbarkeit</li>
</ul>

#HSLIDE
### Konzepte

#VSLIDE
#### Flux
<img width="30%" src="assets/img/flux.svg" alt="Flux Logo">

#VSLIDE
<img width="60%" src="assets/img/flux-flow.svg" alt="Flux unidirectional flow">

#VSLIDE
#### ReactiveX
<img width="30%" src="assets/img/reactive.png" alt="ReactiveX Logo">

#VSLIDE
<img width="100%" src="assets/img/reactive.gif" alt="ReactiveX flow">

#HSLIDE

<img width="30%" src="assets/img/react.png" alt="React Icon">
<img width="30%" src="assets/img/angular.svg" alt="Angular Icon">
<br>
<img width="15%" src="assets/img/redux.png" alt="Redux Icon">
<img width="15%" src="assets/img/mobx.png" alt="MobX Icon">
<img width="15%" src="assets/img/ngrx.png" alt="Ngrx Icon">
<img width="15%" src="assets/img/reactive.png" alt="RxJS Icon">

#HSLIDE
### Bibliotheken

#VSLIDE
<h3>Redux</h3>
<img class="Redux-logo" src="assets/img/redux.png" alt="Redux Logo">

#VSLIDE
<h3>Redux - Eigenschaften</h3>
<ul>
    <li>Flux-Pattern</li>
    <li>Unveränderlichkeit</li>
    <li>"pure" Methoden</li>
    <li>Zentrale Zustandsverwaltung</li>
</ul>

#VSLIDE

<h3>Redux - Root-Reducer</h3>

```js
const rootReducer = combineReducers({
    slideshow: combineReducers({
        users,
        currentUser,
        currentPresentation,
        redirectPath
    }),
    routing: routerReducer,
    i18n: i18nReducer
});
```

#VSLIDE
<!--<h3>Zustandsbaum</h3>-->
<img width="100%" src="assets/img/redux-anwendungszustand.PNG" alt="Redux Zustandsbaum">

#VSLIDE

<h3>Redux - Reducer</h3>

```js
const users = (state = [], action) => {
    switch (action.type) {
        case 'FETCH_ALL_USERS_SUCCESS': {
            return action.payload;
        }
        case 'DELETE_USER_SUCCESS': {
            return state.filter(user => user.id !== action.payload.id);
        }
        default:
            return state;
    }
}

```

#VSLIDE
<h3>Redux - Warum Unveränderlichkeit?</h3>

#VSLIDE

```js
const person = {
  name: 'John',
  age: 28
}
const newPerson = person

newPerson.age = 30
console.log(newPerson === person) // true
console.log(person) // { name: 'John', age: 30 }

```

&#10148; keine Unveränderlichkeit <!-- .element: class="fragment" data-fragment-index="4" -->


#VSLIDE

```js
const person = {
  name: 'John',
  age: 28
}
const newPerson = Object.assign({}, person, {
  age: 30
})

console.log(newPerson === person) // false
console.log(person) // { name: 'John', age: 28 }
console.log(newPerson) // { name: 'John', age: 30 }

```

&#10148; Unveränderlichkeit <!-- .element: class="fragment" data-fragment-index="4" -->

#VSLIDE

```js
const person = {
  name: 'John',
  age: 28
}
const newPerson = { ...person, age: 30 }

console.log(newPerson === person) // false
console.log(person) // { name: 'John', age: 28 }
console.log(newPerson) // { name: 'John', age: 30 }

```

&#10148; Object Spread Operator <!-- .element: class="fragment" data-fragment-index="4" -->

#VSLIDE
<h3>Redux - Vorteile der Unveränderlichkeit</h3>

- Vermeiden von Fehlern
- Nachvollziehbarkeit
- bessere Performance

#VSLIDE
<h3>Redux - "Pure" Methoden</h3>

- Methoden ohne Seiteneffekte
- Selber Input liefert immer denselben Output
- Einfache Testbarkeit


#VSLIDE
<h3>"Pure"?</h3>
```js
Math.random(); // => 0.4011148700956255
Math.random(); // => 0.8533405303023756
Math.random(); // => 0.3550692005082965
```

&#10148; Nicht "pure"! <!-- .element: class="fragment" data-fragment-index="2" -->

#VSLIDE

```js
function square(x) {
   return x * x;
}
```

&#10148; "Pure"! <!-- .element: class="fragment" data-fragment-index="2" -->

#VSLIDE
<h3>Warum "pure"?</h3>

- Nachvollziehbarkeit
- Testbarkeit
- Vorhersehbarkeit

#VSLIDE
<h3>Redux - Vorteile</h3>
<ul>
    <li>Transparenz</li>
    <li>Struktur für eine React-basierte Anwendung</li>
    <li>Zentrale Zustandsverwaltung</li>
    <li>Einfache Testbarkeit</li>
    <li>Viele Entwicklerwerkzeuge</li>
</ul>

#VSLIDE
<h3>Redux - Nachteile</h3>
<ul>
    <li>Sehr viel zusätzlicher Code</li>
    <li>Keine Trennung von Model- und View Zuständen</li>
    <li>Hoher Lernaufwand</li>    
</ul>

#VSLIDE
<h3>MobX</h3>
<img width="30%" src="assets/img/mobx.png" alt="MobX Logo">

#VSLIDE
<h3>MobX - Eigenschaften</h3>
<ul>
    <li>Mehrere Stores</li>
    <li>Observables</li>        
</ul>

#VSLIDE

```js
class TodoStore {
    @observable todos = [];
    
    @computed get completedTodos() {        
        return this.todos.filter(todo => this.completed);
    }

    @action completeTodo(todo) {
        this.todos.map((t) => {
            if (t.id === todo.id) {
                t.complete = !t.complete;
            }
        });
    }
}

@observer
class TodoList extends React.Component {
    
    complete(todo) {
        this.props.store.completeTodo(todo);
    }

    render() {
        const { todos } = this.props.store;

        const todoList = todos.map(todo => (
            <TodoItem key={ todo.id } todo={ todo } />
        ));

        return (
            <div>
                <h1>todos</h1>                
                <ul>{ todoList }</ul>        
            </div>
        )
    }
}
```

#VSLIDE
<h3>MobX - Vorteile</h3>
<ul>
    <li>Sehr unaufdringliche Architektur, viele Freiheiten</li>
    <li>Einfache Testbarkeit</li>
    <li>Niedriger Lernaufwand</li>
    <li>Entwicklerwerkzeuge sind vorhanden</li>    
</ul>

#VSLIDE
<h3>MobX - Nachteile</h3>
<ul>
    <li>Wenig Transparenz über Veränderung der Zustände</li>
    <li>Keine Übersicht über alle Zustände</li>
    <li>Zustände sind dezentral organisiert</li>    
</ul>

#VSLIDE
<h3>RxJS</h3>
<img width="30%" src="assets/img/reactive.png" alt="RxJS Logo">

#VSLIDE
<h3>2 Konzepte</h3>

#VSLIDE
<h3>BehaviorSubjects in mehreren Stores</h3>

#VSLIDE
<h3>RxJS#1 - Eigenschaften</h3>
<ul>
    <li>Mehrere Stores</li>
    <li>Observables</li>
    <li>Typisierung</li>
</ul>

#VSLIDE
<img width="100%" src="assets/img/rxjs_multiple_stores.png" alt="RxJS concept">

#VSLIDE

```js
abstract class Store {
    protected readonly store: Rx.BehaviorSubject<any>;
    readonly changes: Rx.Observable<any>;
    readonly initialState: any;

    protected constructor(initialState: any) {
        this.initialState = initialState;
        this.store = new Rx.BehaviorSubject<any>(initialState);
        this.changes = this.store
            .asObservable()
            .distinctUntilChanged();
    }
    //...
}

const initialState = {
    currentUser: {},
    currentPresentation: {},        
};

@Injectable()
export class UserStore extends Store {
    constructor() {
        super(initialState);
    }
    //...
}

@Component({...})
export class UserProfileComponent implements OnInit, OnDestroy {
    currentUser: User;    
    currentUserSubscription: Subscription;

    constructor(private userStore: UserStore) { }

    ngOnInit() {
        this.currentUserSubscription = this.userStore.currentUser.subscribe(user => {
            console.log(user);
        }            
    }

    ngOnDestroy(): void {        
        this.currentUserSubscription.unsubscribe();
    }
}

```

#VSLIDE
<h3>RxJS#1 - Vorteile</h3>
<ul>
    <li>Injizieren der Stores durch Dependency Injection</li>
    <li>Wenig zusätzlicher Code</li>
    <li>Unveränderlichkeit (Immutability) des Anwendungszustands möglich</li>
    <li>Schnelle Einarbeitung neuer bzw. unerfahrener Mitarbeiter</li>    
</ul>

#VSLIDE
<h3>RxJS#1 - Nachteile</h3>
<ul>
    <li>keine Übersicht über den globalen Anwendungszustand, da über mehrere Stores verteilt</li>
    <li>Stores können sich nicht gegenseitig aufrufen</li>
    <li>Keine zusätzlichen Entwicklerwerkzeuge</li>
</ul>

#VSLIDE
<h3>Implementierung des Redux Patterns</h3>

#VSLIDE
<h3>RxJS#1 - Eigenschaften</h3>
<ul>
    <li>Reducer</li>
    <li>Actions</li>
    <li>Zustandsbaum</li>
    <li>Typisierung</li>
    <li>Observables</li>
</ul>

#VSLIDE
<h3>Konzept</h3>
<img width="40%" src="assets/img/RxJS-create-statetree.png" alt="RxJS zip-Flow">

#VSLIDE

```ts
const appStateObserver: Observable<AppState> =
    userReducer(initState.userState.users, actions).zip(
        currentUserReducer(initState.userState.currentUser, actions),
        presentationModeReducer(initState.presentationState.presentationMode, actions),
        loggedInReducer(initState.loginState.loggedIn, actions),
        i18nReducer(initState.i18nState.locale, actions),
        isUnsavedReducer(initState.presentationState.isUnsaved, actions),
        editPresentationReducer(initState.presentationState.currentPresentation, actions),
    ).map(combine);


const combine = (state): AppState => ({
    userState: {
        users: state[0],
        currentUser: state[1]
    },
    presentationState: {
        presentationMode: state[2],
        currentPresentation: state[6],
        isUnsaved: state[5],
    },
    loginState: {
        loggedIn: state[3],
    },
    i18nState: {
        locale: state[4],
    }
});
```

#VSLIDE
<h3>RxJS#2 - Bewertung</h3>
<ul>
    <li>Redux-Pattern kann über RxJS implementiert werden</li>
    <li>sehr gute Typsicherheit</li>
    <li>Umständlich, aufwändig</li>
    <li>Überambitioniert</li>
</ul>

#VSLIDE
<h3>@ngrx/store</h3>
<img width="30%" src="assets/img/ngrx.png" alt="Ngrx Logo">

#VSLIDE
<h3>@ngrx/store - Eigenschaften</h3>
<ul>
    <li>Redux</li>
    <li>optimiert für Angular</li>    
    <li>basiert auf RxJS</li>
</ul>

#VSLIDE
<img width="100%" src="assets/img/ngrx-supercharge.png" alt="Ngrx Supercharge">

#VSLIDE

```ts
export const store = StoreModule.provideStore(
    {
        todolist: todolistReducer,
        filter: filterReducer,
    },
    {
        todolist: [{ id: 900, text: 'hallo welt', completed: false }],
        filter: '',
    }
);

this.todolist$ = store.select('todolist');
this.filter$ = store.select('filter');

let subscription = this.filter$.subscribe(
    value => this.filter = value
);

```

#VSLIDE
<h3>@ngrx/store - Vorteile</h3>
<ul>
    <li>Nachvollziehbarkeit über den Anwendungszustand</li>
    <li>zentrale Zustandsverwaltung</li>
    <li>Einfache Testbarkeit</li>
    <li>Entwicklerwerkzeuge vorhanden</li>    
    <li>Typisierter Anwendungszustand</li>    
</ul>

#VSLIDE
<h3>@ngrx/store - Nachteile</h3>
<ul>
    <li>Viel zusätzlicher Code</li>
    <li>Keine Trennung von View- und Model-Zuständen</li>
    <li>Kleine Community</li>
    <li>keine offizielle Dokumentation</li>    
</ul>

#HSLIDE

<img width="30%" src="assets/img/react.png" alt="React Icon">
<img width="30%" src="assets/img/angular.svg" alt="Angular Icon">
<br>
<img width="15%" src="assets/img/redux.png" alt="Redux Icon">
<img width="15%" src="assets/img/mobx.png" alt="MobX Icon">
<img width="15%" src="assets/img/ngrx.png" alt="Ngrx Icon">
<img width="15%" src="assets/img/reactive.png" alt="RxJS Icon">

<!--#VSLIDE
<img width="100%" src="assets/img/frameworks.PNG" alt="Github Frameworks">-->


#HSLIDE
### Evaluation

#VSLIDE
<img src="assets/img/slideshow.PNG" alt="Slideshow Screenshot">

#HSLIDE?image=assets/img/start.jpg
### Ergebnisse

#VSLIDE?image=assets/img/start.jpg
<h3>Zusätzlicher Code</h3>
<ul>
    <li>Transparenz und Nachvollziehbarkeit</li>
    <li>Übersicht über die Architektur</li>
    <li>Konventionen vs. Freiheiten</li>
    <li>Einfachheit</li>
</ul>

#VSLIDE?image=assets/img/start.jpg
<h3>Frameworks</h3>
<ul>
    <li>Integrierte Werkzeuge</li>
    <li>Properties</li>
    <li>Services / DI</li>
    <li>RxJS</li>
</ul>

#VSLIDE?image=assets/img/start.jpg
<h3>Unveränderlichkeit</h3>
<ul>
    <li>!== Redux</li>
    <li>Immutable.js</li>
    <li>Object.assign, Object Spread Operator, Array Funktionen (filter, map, etc.)</li>
    <li>Object.freeze()</li>    
</ul>

#VSLIDE?image=assets/img/start.jpg
<h3>"pure" Methoden</h3>
<ul>
    <li>!== Redux</li>
    <li>Einfachheit</li>
    <li>Nachvollziehbarkeit</li>
    <li>Testbarkeit</li>
</ul>

#VSLIDE?image=assets/img/start.jpg
<h3>Entwicklerwerkzeuge</h3>
<ul>
    <li>`console.log` ist ausreichend</li>
    <li>Hot Reloading spart Zeit, aber nicht immer</li>    
</ul>

#VSLIDE?image=assets/img/start.jpg
<h3>Community</h3>
<ul>
    <li>Github</li>
    <li>Stack Overflow</li>
</ul>

#HSLIDE?image=assets/img/pexels-photo-220357.jpeg
<h2 style="color: rgb(185, 196, 0) !important">Fazit</h2>

<!--Redux und RxJS -> am besten irgendetwas dazwischen -->